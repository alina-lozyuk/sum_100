let arrNumber = [2,3,4,5,6,7,8,9];
let expression = "1";

function considerExpression(expression){
    let inputExpr = expression;
    let result = 0;
    while (inputExpr.length != 0){
        let posPlus = ( inputExpr.indexOf("+", 1) == -1) ? inputExpr.length : inputExpr.indexOf("+", 1);
        let posMinus = ( inputExpr.indexOf("-", 1) == -1) ? inputExpr.length : inputExpr.indexOf("-", 1);
        let countSymbol = (posPlus > posMinus) ? posMinus : posPlus;
        let currNumber = inputExpr.substr(0, countSymbol);
        inputExpr = inputExpr.slice(countSymbol);
        result +=  parseInt(currNumber);
    }
    return result;
}

function createExpression(expression, arrNumber) {
    if (arrNumber.length == 0){
        let resultExpression = considerExpression(expression);
        if (resultExpression == 100) {
            console.log(expression + "     " + resultExpression);
        }
        return
    }
    let addElement = arrNumber.shift();
    createExpression(expression + "+" + addElement, arrNumber.slice());
    createExpression(expression + "-" + addElement, arrNumber.slice());
    createExpression(expression + addElement, arrNumber.slice());
}

createExpression(expression, arrNumber);